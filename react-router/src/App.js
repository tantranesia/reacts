import React, { useState } from 'react';
import Home from "./pages/Home.js"
import About from "./pages/aboutme.jsx"
import Contact from "./pages/Contact.js";
import Portfolio from "./pages/portfolio.js"
import List from "./pages/apptodo.jsx"
import Login from "./pages/login.jsx"
import { Switch, Route, Link, BrowserRouter } from "react-router-dom";
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, NavbarText} from 'reactstrap';
import './App.css';
import "./bootstrap.css"

function App() {
  return (
    <BrowserRouter>
    <Switch>
    <div className="App">
    <h1>Ary Tantranesia</h1>
      <Nav>
        <NavItem>
          <NavLink href="/home">Home</NavLink>
        </NavItem>
        <NavItem>
          <NavLink href="/about">About</NavLink>
        </NavItem>
        <NavItem>
          <NavLink href="/contact">Contact</NavLink>
        </NavItem>
        <NavItem>
          <NavLink href="/portfolio">Portfolio</NavLink>
        </NavItem>
      </Nav>
      <hr />

      
        <Route path="/about">
          <About />
        </Route>
        <Route path="/contact">
          <Contact />
        </Route>
        <Route path="/home">
          <Home />
        </Route>
        <Route path="/portfolio">
          <Portfolio />
        </Route>
        <Route path="/list">
              <List />
        </Route>
        <Route path="/login">
              <Login />
        </Route>
        </div>
      </Switch>
      </BrowserRouter>
    
  );
};

export default App