import React from "react";
import "../bootstrap.css";
import "../Blog CSS/blog.css"

const About = () => {
  return <div>
        <nav class="navbar fixed-top navbar-light bg-light">
             <a class="navbar-brand" href="#">Ary Tantranesia</a>
        </nav>  
        <h2 class="container-about-me" id="demo" onclick="myFunction()">Ary says hi!</h2>
        <img class="image-about" src="https://media0.giphy.com/media/WpIPS0DWNpMm4kfMVr/giphy.gif?cid=ecf05e47qrvro3jbf0pfiffr9phphdby4symxchfhe6mgmko&rid=giphy.gif"></img>
        <main>
            <div class="containermain">
                <h2>Your mood today: <br></br> <span class="color" id="c.text">Start!</span></h2>
                <div class="buttoncentre">
                    <button class="btn btn-hero" id="btn">Click me</button>
                </div>
            </div>
        </main>
        <div class="row">
        <div class="col-md-6">
            <div class="card" style={{width: "25rem"}}>
                <img src="https://www.linkpicture.com/q/LPic5fc4673062fa2918443214.jpg" class="card-img-top" alt="Movie Recommendations"></img>
                    <div class="card-body">
                     <p class="card-text">Click to see my movies recommendations!</p>
                </div>
             </div>
        </div>
        <div class="col-md-6">
            <div class="card" style={{width: "25rem"}}>
                <img src="https://www.linkpicture.com/q/victor-rodvang-rbSpGv1wb5M-unsplash.jpg" class="card-img-top" alt="My Playlist"></img>
                     <div class="card-body">
                         <p class="card-text">Click to see my playlist!</p>
                    </div>
            </div>
        </div>
    </div>
  </div>
  
};

export default About;