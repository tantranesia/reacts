import React, { useState } from "react";
import "../bootstrap.css";
import "../Blog CSS/blog.css"


const Aboutme = () => {
const [ color, setColor] = useState(" ")
const [ text, setText ] = useState(" ")

    const changeStyle = () =>{
        const colors = ["#FFB6C1", "#FF7F50", "#87CEFA", "#FFD700", "#90EE90", "#A0522D", "#808080"];
        const texts = ["Love", "Excited", "Calm", "Creative", "Adventorous", "Simple", "Sad"];
        let bcg = document.body.style.backgroundColor;
        let color = colors[Math.floor(Math.random() * colors.length)]
        console.log(color, "ini color")
        let textss = texts[Math.floor(Math.random() * texts.length)]
        console.log(text, "ini text")
        if (bcg === colors[color]){
            changeStyle();
        } else {
            setColor(color)
            setText(textss)
            console.log(color)
            // document.body.style.backgroundColor = colors[color]; 
            // document.getElementById("c.text").innerHTML = text[texts];
            
        }
    }
    console.log(color)
    return ( 
    <div style={color != " " ? {background: color} : null}>
        <h2 class="container-about-me" id="demo" onclick="myFunction()">Ary says hi!</h2>
        <img class="image-about" src="https://media0.giphy.com/media/WpIPS0DWNpMm4kfMVr/giphy.gif?cid=ecf05e47qrvro3jbf0pfiffr9phphdby4symxchfhe6mgmko&rid=giphy.gif"></img>
        <main>
            <div class="containermain">
                <h2>Your mood today: <br></br> <span class="color" id="c.text">{text != " " ? text : null}</span></h2>
                <div class="buttoncentre">
                    <button class="btn btn-hero" id="btn" onClick={() => changeStyle()} >Click me</button>
                </div>
            </div>
        </main>
        
        <div class="row">
        <div class="col-md-6">
            <div class="card" style={{width: "25rem"}}>
                <img src="https://www.linkpicture.com/q/LPic5fc4673062fa2918443214.jpg" class="card-img-top" alt="Movie Recommendations"></img>
                    <div class="card-body">
                     <p class="card-text">Click to see my movies recommendations!</p>
                </div>
             </div>
        </div>
        <div class="col-md-6">
            <div class="card" style={{width: "25rem"}}>
                <img src="https://www.linkpicture.com/q/victor-rodvang-rbSpGv1wb5M-unsplash.jpg" class="card-img-top" alt="My Playlist"></img>
                     <div class="card-body">
                         <p class="card-text">Click to see my playlist!</p>
                    </div>
            </div>
        </div>
    </div>
</div>
    )

}

export default Aboutme