import React, { useState } from "react" // import useState
import Styles from "./index.module.css";

const PracticeState = () => {
  // ini nama function
  const [isLogged, setIsLogged] = useState(true) // define state
  const handleClick = (e) => {
    //triger onclick
    setIsLogged(!isLogged) // change state this.bind
  }
  return (
    <div className={Styles.container}>
       <h1 className={Styles.h1}>You are logged {isLogged ? "in" : "out"}</h1>
      <button className={Styles.clickbutton} onClick={(e) => handleClick(e)}>Click</button>
   </div>
      

  )
}

export default PracticeState