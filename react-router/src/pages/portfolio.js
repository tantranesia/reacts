import React, { useState } from 'react';
// import React from "react";
import "../bootstrap.css"
import Style from "../App.css";
import { Table } from 'reactstrap';
import "./index.module.css"
import { Switch, Route, Link, BrowserRouter } from "react-router-dom";

const Portfolio = (props) => {
  return ( <div>
       <nav class="navbar fixed-top navbar-light bg-light">
            <a class="navbar-brand" href="#">Ary Tantranesia</a>
        </nav>
        <h2> I've involed in many project before, as content writer or as a front-end developer. You can check my project below:</h2>
        <br></br>
    <Table>
      <thead>
        <tr>
          <th>#</th>
          <th>Project Name</th>
          <th>Content</th>
          <th>Link</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">1</th>
          <td>Bitcoin</td>
          <td>Article</td>
          <td><a href="https://coinvestasi.com/belajar/apakah-trading-bitcoin-legal/">Apakah Trading Bitcoin Legal?</a></td>
        </tr>
        <tr>
          <th scope="row">2</th>
          <td>Trading</td>
          <td>Article</td>
          <td><a href ="https://coinvestasi.com/belajar/apa-itu-indikator/">Apa itu Indikator dalam Trading?</a></td>
        </tr>
        <tr>
          <th scope="row">3</th>
          <td>React Practice</td>
          <td> Simple To Do List</td>
          <td><Link to="/list">React Practice </Link> </td>
          </tr>
          <tr>
          <th scope="row">4</th>
          <td>React Practice</td>
          <td> Simple Log In/Out</td>
          <td><Link to="/login">React Practice </Link> </td>
          </tr>
      </tbody>
    </Table>
    </div>
  );
}

export default Portfolio;