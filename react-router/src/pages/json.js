import PracticeState from "./login";

export const dataJson = JSON.stringify([
    {
        name: "Practice",
        status: true,
        checkbox: false,
        edited: false
    },
    {
        name: "Cook",
        status: true,
        checkbox: false,
        edited: false
    },
    {
        name: "Cry",
        status: true,
        checkbox: false,
        edited: false
    }
]);