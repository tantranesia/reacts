import "./components/index.module.css";
import "./components/bootstrap.css";
import List from "./components/index.jsx";
import Navbar1 from "./components/navbar.jsx"
import Carousel from "./components/carousel.jsx"
import PracticeState from './components/login';
import Todo from "./components/apptodo.jsx"


function App() {

  return (
    <div className="App">
      <body>
      <Navbar1/>
       <br></br>
      <List/>
      <br></br>
      <br></br>
      <br></br>
      <Carousel/>
      <br></br>
      <PracticeState/>
      <br></br>
      <hr></hr>
      <Todo />
  
      </body>
       
     
      
    </div>
  );
}

export default App;
