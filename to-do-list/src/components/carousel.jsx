import Styles from "./index.module.css";
import React, { useState } from 'react';
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption
} from 'reactstrap';

const items = [
  {
    src: 'https://res.cloudinary.com/doqopynok/image/upload/v1607020133/photo_2020-12-04_01.27.34_boudcu.jpg',
    altText: 'Slide 1',
    caption: 'Japan',
  },
  {
    src: 'https://res.cloudinary.com/doqopynok/image/upload/v1607020132/photo_2020-12-04_01.27.27_zbsup6.jpg',
    altText: 'Slide 2',
    caption: 'Japan'
  },
  {
    src: 'https://res.cloudinary.com/doqopynok/image/upload/v1607020133/photo_2020-12-04_01.27.36_c3cjzu.jpg',
    altText: 'Slide 3',
    caption: 'Japan'
  }
];

const Carousel1 = (props) => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);

  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  }

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  }

  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  }

  const slides = items.map((item) => {
    return (
      <CarouselItem
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
        key={item.src}
      >
        <img src={item.src} alt={item.altText} />
        <CarouselCaption captionText={item.caption} captionHeader={item.caption} style={{ width: "50%"}}/>
      </CarouselItem>
    );
  });

  return (
    <Carousel
      activeIndex={activeIndex}
      next={next}
      previous={previous}
    >
      <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} />
      {slides}
      <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
      <CarouselControl direction="next" directionText="Next" onClickHandler={next} />
    </Carousel>
  );
}

export default Carousel1;