import { dataJson } from "./json"
import React, { useState, useEffect } from "react"
import { Input } from "reactstrap";
import Styles from "./index.module.css";
import { RestOutlined } from '@ant-design/icons';


const List = (props) => {
const [arr, setArr] = useState ([])
const [todo, setTodo] = useState(" ")
const [index, setIndex] = useState(0)
const removeKey = async (e) => {
const newTodos = await arr.filter((item) => item !== arr[e])
    setArr(newTodos)   
}
const handleChange = (e, x = null) => {
    const { name, value }= e.target
    setTodo(value)
}
const submit = (e = null) => {

    setArr((prev) => [...arr, { name: todo, status: false }])
    setIndex(index + 1)

}

useEffect(() => {
    console.log("useeffect")
    if (arr.length <= 0) {
      console.log("data")
      console.log(dataJson, "masih string dan belum di parsing")
      console.log(JSON.parse(dataJson), "sudah di parsing berbentuk object")
      console.log("data tutup")
      setArr(JSON.parse(dataJson))
    }
  }, [])
  
      return (
        <div>
            <div className={Styles.container}>
                <h1 className={Styles.h1}> My To Do List</h1>
                <div className={Styles.containerinput}>
                    <form>
                        <Input className={Styles.inputtext} name="todoNext" onChange={(e) => { handleChange(e) }} placeolder="To do text" value={todo} type="textarea" name="text" id="exampletext"/>
                        <div className={Styles.containerbutton}>
                            <button className={Styles.buttonsubmit} onClick={() => submit()}>Submit</button>
                        </div>
                        
                    </form>
                </div>
                <div className={Styles.label}>
                    {Object.keys(arr).map((keyname, i) => (
                    <div> 
                        <form>
                            <label check><input type="checkbox" /> {arr[keyname]["name"]}
                            </label>
                            <div className={Styles.buttonstyle}>
                                <button className={Styles.buttonerase} onClick={() => removeKey(i)} ><RestOutlined /> Delete
                                </button>
                            </div>
                            <hr/>
                        </form>
                    </div>
                    ))}
                </div>
                
            </div>
        </div>
      )
  }
  
  export default List