import Styles from "./index.module.css";
import "./bootstrap.css";
import { Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    NavbarText, Spinner } from 'reactstrap';
    import React, { useState } from 'react';

    const Navbar1 = (props) => {
        const [isOpen, setIsOpen] = useState(false);
      
        const toggle = () => setIsOpen(!isOpen);
      
        return (
          <div>
            <Navbar color="light" light expand="md">
              <NavbarBrand href="/">reactstrap</NavbarBrand>
              <NavbarToggler onClick={toggle} />
              <Collapse isOpen={isOpen} navbar>
                <Nav className="mr-auto" navbar>
                  <NavItem>
                    <NavLink href="/components/">Components</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink href="https://github.com/reactstrap/reactstrap">Donate</NavLink>
                  </NavItem>
                  <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle nav caret>
                      Social Media
                    </DropdownToggle>
                    <DropdownMenu right>
                      <DropdownItem>
                        Twitter
                      </DropdownItem>
                      <DropdownItem>
                        Instagram
                      </DropdownItem>
                      <DropdownItem divider />
                      <DropdownItem>
                        Blog
                      </DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </Nav>
                <NavbarText>Ary Tantranesia</NavbarText>
              </Collapse>
            </Navbar>
    
          </div>
        )
      }

export default Navbar1