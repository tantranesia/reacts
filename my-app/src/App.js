import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, Spinner, Container, Row, Col } from 'reactstrap';
import SuperCentered from "./components/Super-Centered";
import Deconstructed from "./components/deconstructed";
import Sidebar from "./components/sidebar";
import Pancake from "./components/pancake";
import Classic from "./components/classic";
import Span from "./components/span";
import Ram from "./components/ram";
import Card from "./components/card";
import Clamping from "./components/clamping";
import Respect from "./components/respect";
import List from "./components 2/index";
import Header from "./components 2//index2";
import Navbar1 from "./components 2/index3";

function App() {
  return (
    <div className="App">
      <ol>
      <h1>Super Centered</h1>
    <SuperCentered/>
    <br></br>
    <hr></hr>

    <h1>The Deconstructed Pancake</h1>
    <Deconstructed/>
    <br></br>
    <hr></hr>

    <h1>Sidebar Says</h1>
    <Sidebar/>
    <br></br>
    <hr></hr>

    <h1>Pancake Stack</h1>
    <Pancake/>
    <br></br>
    <hr></hr>

    <h1> Classic Holy Grail Layout</h1>
    <Classic/>
    <br></br>
    <hr></hr>

    <h1>12-Span Grid</h1>
    <Span/>
    <br></br>
    <hr></hr>

    <h1>RAM (Repeat, Auto, Minmax)</h1>
    <Ram/>
    <br></br>
    <hr></hr>

    <h1>Line Up</h1>
    <Card/>
    <br></br>
    <hr></hr>

    <h1>Clamping My Style</h1>
    <Clamping/>
    <br></br>
    <hr></hr>

    <h1>Respect for Aspect</h1>
    <Respect/>
    <br></br>
    <hr></hr>
    </ol>
    <br></br>
    <br></br>
    <h1>New Assigment</h1>
    <List/>
    <br></br>
    <Header/>
    <br></br>
    <Navbar1/>
    </div>
  );
}

export default App;
