import Styles from "../assets/deconstructed.module.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, Spinner, Container, Row, Col } from 'reactstrap';
import React from "react";

class Deconstructed extends React.Component {
    render() {
      return <div className={Styles.body}>
      <div className={Styles.parent}>
  <div className={Styles.child}>1</div>
  <div class={Styles.child}>2</div>
  <div class={Styles.child}>3</div>
  </div>
</div>;
    }
}
export default Deconstructed