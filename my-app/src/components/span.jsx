import Styles from "../assets/span.module.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, Spinner, Container, Row, Col } from 'reactstrap';
import React from "react";

class Span extends React.Component {
    render() {
      return <div className={Styles.body}>
<div className={Styles.span12}>Span 12</div>
<div class={Styles.span6}>Span 6</div>
<div class={Styles.span4}>Span 4</div>
<div class={Styles.span2}>Span 2</div>
</div>
    }
}

export default Span