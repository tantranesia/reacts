import Styles from "../assets/classic.module.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, Spinner, Container, Row, Col } from 'reactstrap';
import React from "react";
class Classic extends React.Component {
    render() {
      return <div className={Styles.body}>
<header className={Styles.header}><h1 contenteditable>Header.com</h1></header>
<div className={Styles.leftsidebar} contenteditable>Left Sidebar</div>
<main className={Styles.main} contenteditable></main>
<div className={Styles.rightsidebar} contenteditable>Right Sidebar</div>
<footer className={Styles.footer} contenteditable>Footer Content — Header.com 2020</footer>
</div>
    }
}

export default Classic