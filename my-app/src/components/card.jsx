import Styles from "../assets/card.module.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, Spinner, Container, Row, Col } from 'reactstrap';
import React from "react";

class Card extends React.Component {
    render() {
      return <div className={Styles.body}><div className={Styles.card}>
  <h1 className={Styles.h1}>Title - Card 1</h1>
  <p>Medium length description. Let's add a few more words here.</p>
  <div className={Styles.visual}></div>
</div>
<div className={Styles.card}>
  <h1 className={Styles.h1}>Title - Card 2</h1>
  <p>Long Description. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sed est error repellat veritatis.</p>
  <div className={Styles.visual}></div>
</div>
<div className={Styles.card}>
  <h1 className={Styles.h1}>Title - Card 3</h1>
  <p>Short Description.</p>
  <div className={Styles.visual}></div>
</div>
</div>
    }
}

export default Card