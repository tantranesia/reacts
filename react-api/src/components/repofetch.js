import React, { Component } from "react";
import { Container } from 'reactstrap';

 
class Fetch extends Component {
   constructor(props){
       super(props)
       this.state = {
        repos: []
       }
   }

   componentDidMount(){
       fetch("https://api.github.com/users/tantranesia/repos")
        .then((res) => res.json())
        .then((repos) => {
            this.setState({repos: repos});
            console.log(repos)
        });
   }

   render (){
    const{repos} = this.state
    return (
        <div class="boxWhite">
            <div class="container">
                <h1>Repo Available:</h1>
                <h2 class="font-ul">
                { this.state.repos.map(repos => <p>{repos.name}</p>)}
                { this.state.repos.map(repos => <p>{repos.descriptions}</p>)}
                </h2>
                <br></br>
            </div>
        </div>
    )
   }
}

export default Fetch;