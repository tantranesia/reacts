import React, { Component } from "react";

 
class Germany extends Component {
   constructor(props){
       super(props)
       this.state = {
        items: []
       }
   }

   componentDidMount(){
       fetch("https://randomuser.me/api/?results=10&nat=de")
        .then(res => res.json())
        .then(parsedJSON => parsedJSON.results.map(data => (
            {
                id: `${data.id.name}`,
                firstName: `${data.name.first}`,
                lastName: `${data.name.last}`,
                location : `${data.location.state}, ${data.nat}`,
                thumbnail: `${data.picture.large}`
            }
        )))
        .then(items => this.setState({
            items
        }))
        .catch(error => console.log('parsing data is failed', error))
   }

   render(){
       const{items} = this.state
       return(
           <div className="boxWhite">
               <h2>Random User</h2>
               {
                   items.length > 0 ? items.map(item => {
                       const {id, firstName, lastName, location, thumbnail} = item
                       return(
                        <div key={id} className="bgCircle">
                        <center><img src={item.thumbnail} alt={thumbnail} className="circle"></img></center>
                        <center><p>{item.firstName} {item.lastName}</p></center>
                        <center><p>{item.location}</p></center>
                               
                           </div>
                       )
                }) : null
               }
           </div>
       )
   }
}

export default Germany;