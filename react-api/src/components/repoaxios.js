import React from "react"
import axios from 'axios';
import { Container } from 'reactstrap';

export default class Axios extends React.Component {
    state = {
      repos: []
    }
  
    componentDidMount() {
      axios.get(`https://api.github.com/users/tantranesia/repos`)
        .then(res => {
          const repos = res.data;
          this.setState({ repos });
        })
    }
  
    render() {
      return (
        <div class="boxWhite">
            <div class="container">
                <h1>Repo Available:</h1>
                <h2 class="font-ul">
                { this.state.repos.map(repos => <p>{repos.name}</p>)}
                { this.state.repos.map(repos => <p>{repos.descriptions}</p>)}
                </h2>
                <br></br>
            </div>
        </div>
      )
    }
  }


