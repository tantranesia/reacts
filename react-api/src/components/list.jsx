import React from "react"
const Loading = (props) => {
    const {repos} = props;
    if( !repos || repos.length === 0) return <p> Data not found </p>
    return 
    <h1> Available Repository</h1>
    {repos.map((repo) => {
        return (
            <li key={repo.id}> 
                <p>{repo.name}</p>
                <p>{repo.descriptions}</p>
            </li>
        )
    })}
}
export default Loading