import logo from './logo.svg';
import "./style.css"
import { Breadcrumb, BreadcrumbItem, Table } from 'reactstrap';
import { Switch, Route, Link, BrowserRouter } from "react-router-dom";
import Random from "./components/Home"
import Brazil from "./components/Brazil"
import Turkey from "./components/Turkey"
import Germany from "./components/Germany"
import Fetch from "./components/repofetch"
import Axios from "./components/repoaxios"
import Home from './components/Home';

function App() {
  return (
    <BrowserRouter>
    <Switch>
    <div className="App">
      <Table>
      <thead>
        <tr class="table">
          <th>#</th>
          <th>Project Name</th>
          <th>Link</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">1</th>
          <td>Random</td>
          <td class="link"><Link target={"_blank"} to="/random">Random </Link> </td>
        </tr>
        <tr>
          <th scope="row">2</th>
          <td>Brazil</td>
          <td><Link target={"_blank"} to="/brazil">Brazil </Link> </td>
        </tr>
        <tr>
          <th scope="row">3</th>
          <td>Turkey</td>
          <td><Link target={"_blank"} to="/turkey">Turkey </Link> </td>
          </tr>
          <tr>
          <th scope="row">4</th>
          <td>Germany</td>
          <td><Link target={"_blank"} to="/germany">Germany </Link> </td>
          </tr>
          <tr>
          <th scope="row">5</th>
          <td>Fetch</td>
          <td><Link target={"_blank"} to="/fetch">Fetch </Link> </td>
          </tr>
          <tr>
          <th scope="row">6</th>
          <td>Axios</td>
          <td><Link target={"_blank"} to="/axios">Axios </Link> </td>
          </tr>
      </tbody>
    </Table>
        <Route path="/random">
            <Random />
        </Route>

        <Route path="/brazil">
            <Brazil />
        </Route>
        <Route path="/turkey">
            <Turkey />
        </Route>
        <Route path="/germany">
            <Germany />
        </Route>
        <Route path="/fetch">
            <Fetch />
        </Route>
        <Route path="/axios">
            <Axios />
        </Route>
    
     
    </div>
    </Switch>
    </BrowserRouter>
  );
}

export default App;
